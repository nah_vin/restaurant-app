const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const users = require('./routes/users');
const food = require('./routes/foods');
const order = require('./routes/orders');

const mongodb = require('./config/db.settings');

const port = process.env.PORT || 3000;
const app = express();

mongoose.connect(mongodb.db);

mongoose.connection.on('connected',(connect)=>{
    console.log('MongoDB connected successfully !!' + mongodb.db);
});

mongoose.connection.on('error',(error)=>{
    console.log('MongoDB connection failed !!' + error);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));


app.use('/api', users);
app.use('/api', food);
app.use('/api', order);

app.get('/', (req,res)=>{
    res.send('Welcome to Restaurant App');
});

app.listen(port,()=>{
    console.log('Server is running at port 3000');
});

module.exports = app 