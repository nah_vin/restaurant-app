const express = require('express');
const router = express.Router();
const food = require('../helpers/foods');
const user = require('../helpers/users');
// Food
router.post('/food',user.verifyToken, food.addFood);
router.get('/allFood', user.verifyToken, food.getAllFoods);
router.get('/food/:name', user.verifyToken, food.getFood);
router.put('/food/:name', user.verifyToken, food.updateFood);
router.delete('/food/:name', user.verifyToken, food.deleteFood);

module.exports = router;
