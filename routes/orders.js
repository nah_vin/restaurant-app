const express = require('express');
const router = express.Router();
const order = require('../helpers/orders');
const user = require('../helpers/users');

// Order
router.post('/order',user.verifyToken,order.saveOrderDetails);
router.get('/orders',user.verifyToken,order.getAllOrders);
router.get('/order/:id',user.verifyToken,order.getOrderById);
router.delete('/order/:id',user.verifyToken,order.deleteOrder);

module.exports = router;
