const express = require('express');
const router = express.Router();
const user = require('../helpers/users');

// Users
router.post('/user', user.registerUser);
router.get('/users', user.verifyToken, user.getAllUsers);
router.get('/user/:email', user.verifyToken, user.getUser);
router.put('/user/:email', user.verifyToken, user.updateUser);
router.delete('/user/:email', user.verifyToken, user.deleteUser);
router.post('/login', user.loginUser);

module.exports = router;
