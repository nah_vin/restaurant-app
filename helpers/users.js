const User = require('../models/User');
const jsonwebtoken = require('jsonwebtoken');
const secretKey = require('../config/db.settings');

module.exports.getAllUsers = async function (req, res) {
    let allUsers = await User.find();
    if (allUsers.length > 0) {
        res.status(200).json({ Users: JSON.parse(JSON.stringify(allUsers)) });
    } else {
        res.status(204).json({  err: "Failed to fetch users lists" });
    }
}

module.exports.registerUser = async function (req, res) {
    var user = new User({
        password: req.body.password,
        email: req.body.email

    })
    let userData = await User.findOne({ email: req.body.email })
    if (userData) {
        res.status(404).json({err: "User already exists" });

    } else {
        user.save().then((savedData) => {
            res.status(200).json({  userInfo: JSON.stringify(savedData) });
        }).catch((err) => {
            res.status(400).json({  err: err });
        })
    }

}

module.exports.getUser = async function (req, res) {
    let userData = await User.findOne({ email: req.params.email });
    if (userData) {
        res.status(200).json({ userInfo: userData });
    } else {
        res.status(404).json({  msg: 'No User details found ' });
    }

}


module.exports.deleteUser = async function (req, res) {
    let userData = await User.findOne({ email: req.params.email });
    if (userData) {
        await User.findOneAndRemove({ email: req.params.email });
        res.status(204).json({userInfo: 'User Details deleted from the db' });
    } else {
        res.status(404).json({ msg: 'No User details found ' });
    }
}

module.exports.updateUser = async function (req, res) {
    let userData = await User.findOne({ email: req.params.email });
    if (userData) {
        let update = await User.findOneAndUpdate({ email: req.params.email }, { $set: req.body })
        res.status(200).json({  userInfo: update });
    } else {
        res.status(404).json({  msg: 'No User details found ' });
    }
}


module.exports.loginUser = async function (req, res) {
    let user = await User.findOne({ email: req.body.email });
    if (user) {
        var token = jsonwebtoken.sign({ email: req.body.email }, secretKey.secret_key, {
            expiresIn: 86400 // expires in 24 hours
        });
        res.status(200).json({ userInfo: JSON.stringify(user), token: token });
    } else {
        res.status(404).json({ err: "User doesn't exist" });
    }

}

module.exports.verifyToken = function (req, res, next) {
    var token = req.headers['access_token'];
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    jsonwebtoken.verify(token, secretKey.secret_key, function (err, decoded) {
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        req.decoded = decoded;
        next();
    });
}
