const Food = require('../models/Food');

module.exports.getAllFoods = async function (req, res) {
    let food = await Food.find();
    if (food.length > 0) {
        res.status(200).json({ Food: JSON.parse(JSON.stringify(food)) });
    } else {
        res.status(404).json({ err: "Failed to fetch food lists" });
    }

}

module.exports.addFood = async function (req, res) {
    var foodDetails = new Food({
        name: req.body.name,
        style: req.body.style,
        spiceLevel: req.body.spiceLevel,
        cost: req.body.cost,
        isVegan: req.body.isVegan
    })
    let food = await Food.findOne({ name: req.body.name })
    if (food) {
        res.status(404).json({ err: "Food already exists" });

    } else {
        foodDetails.save().then((savedData) => {
            res.status(200).json({ foodInfo: JSON.stringify(savedData) });
        }).catch((err) => {
            res.status(400).json({ err: err });
        })
    }

}

module.exports.getFood = async function (req, res) {
    let food = await Food.findOne({ name: req.params.name });
    if (food) {
        res.status(200).json({ FoodInfo: food });
    } else {
        res.status(404).json({ msg: 'No Food details found ' });
    }

}


module.exports.deleteFood = async function (req, res) {
    let food = await Food.findOne({ name: req.params.name });
    if (food) {
        await Food.findOneAndRemove({ name: req.params.name });
        res.status(204).json({ foodInfo: 'Food Details deleted from the db' });
    } else {
        res.status(404).json({ msg: 'No Food details found ' });
    }
}

module.exports.updateFood = async function (req, res) {
    let food = await Food.findOne({ name: req.params.name });
    if (food) {
        let update = await Food.findOneAndUpdate({ name: req.params.name }, { $set: req.body })
        res.status(200).json({ foodInfo: update });
    } else {
        res.status(404).json({ msg: 'No Food details found ' });
    }
}




