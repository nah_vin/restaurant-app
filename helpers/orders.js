const order = require('../models/Order');
const Food = require('../models/Food');
const async = require('async')

const queue = async.queue((item, completed) => {
    console.log("Currently Busy Processing Item " + item);
    setTimeout(() => {
        const remaining = queue.length();
        completed(null, { item, remaining });
    }, 1000);
}, 1);

module.exports.saveOrderDetails = async (req, res) => {
    let orderDetails = req.body, saveOrder = []
    let food = await Food.find();

    let price = {}
    for (let i = 0; i < orderDetails.length; i++) {
        food.find((item) => {
            if (item.name === orderDetails[i].name) {
                return saveOrder.push({ "name": item.name, "price": orderDetails[i].quantity * item.cost })
            }
        })
    }

    price.items = saveOrder
    price.total = saveOrder.reduce(function (acc, obj) { return acc + obj.price; }, 0);
    let orderObj = order({
        items: orderDetails,
        total: price.total,
        email: "naveen1@gmail.com"
    })

    // Push order to queue
    queue.push(orderObj, (error, { item, remaining }) => {
        if (error) {
            console.log(`An error occurred while processing  ${orderObj}`);
        } else {
            console.log(`Finished processing ${orderObj}
                   . ${remaining} remaining`);
        }
    });

    // Save order
    orderObj.save().then((orderObj) => {
        res.status(200).json({ Orders: JSON.parse(JSON.stringify(orderObj)) });
    }).catch((err) => {
        res.status(400).json({ err: err });
    })
}

module.exports.getAllOrders = async function (req, res) {
    let orders = await order.find();
    if (orders.length > 0) {
        res.status(200).json({ Orders: JSON.parse(JSON.stringify(orders)) });
    } else {
        res.status(204).json({ err: "Failed to fetch users lists" });
    }
}

module.exports.deleteOrder = async function (req, res) {
    let orderData = await order.findOne({ _id: req.params.id });
    if (orderData) {
        await order.findOneAndRemove({ _id: req.params.id });
        res.status(204).json({ Orderdata: 'Order Details deleted from the db' });
    } else {
        res.status(404).json({ msg: 'No Order details found ' });
    }
}

module.exports.getOrderById = async function (req, res) {
    let orderData = await order.findOne({ _id: req.params.id });
    if (orderData) {
        res.status(200).json({ orderInfo: orderData });
    } else {
        res.status(404).json({ msg: 'No Order details found ' });
    }

}