# Restaurant food order app

## Description

The app allows user to register and login to order food from the restaurant's food menu.

User should login frst with using email and password as required details and use Login api to gennerate token for authentication to access rest of the app's api.

`POST /api/login`
#### Example: 
```json
{
    "email": "newUser@gmail.com",
}
```

Once the token is geneated from the login API, use it as "access_token" in request headers for other API's to interact with db. 



## User model

#### Example: 
```json
{
    "password": "pass",
    "email": "newUser@gmail.com",
}
```


## User Methods

`GET /api/users`

`GET /api/user/:email`

`POST /api/user`

`PUT /api/user/:email`

`DELETE /api/user/:email`


## Food model

#### Example: 
```json
{
    "name":"Biryani",
    "style":"Indian",
    "spiceLevel":"high",
    "cost":250
}
```
## Food Methods

`GET /api/allFood`

`GET /api/food/:name`

`POST /api/food`

`PUT /api/food/:name`

`DELETE /api/food/:name`



## Order model

#### Example: 
```json
[
    {
        "name": "Biryani",
        "quantity": 3
    },
    {
        "name": "Momos",
        "quantity": 2
    }
]
```
## Order Methods

`GET /api/orders`

`GET /api/order/:id`

`POST /api/order`

`DELETE /api/order/:id`


## Usage

`npm install`

`npm start`


## Testing 

`npm run test`


## To build and run docker image for the app

`docker build . -t <user name>/node-app`
`docker images`
`docker run -p 49160:3000 -d <user name>/node-app`
