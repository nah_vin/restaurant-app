const mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    email:{
        type:String, required: true
    },
    password:{
        type:String, required: true
    }

});

var userSchema = module.exports = mongoose.model('User',userSchema);
