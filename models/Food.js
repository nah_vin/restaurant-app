const mongoose = require('mongoose');

var foodSchema = mongoose.Schema({
    name: {
        type: String, required: true
    },
    style: {
        type: String, required: true
    }, 
    spiceLevel: {
        type: String, required: true
    },
    cost: {
        type: Number, required: true
    },
    isVegan: {
        type: Boolean, required: true
    }
});

var foodSchema = module.exports = mongoose.model('Food', foodSchema);
