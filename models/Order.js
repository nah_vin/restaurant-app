const mongoose = require('mongoose');
const Schema = mongoose.Schema;

itemSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    quantity: {
        type: Number,
        required: true
    }
})

OrderSchema = new Schema({
    items: {
        type: [itemSchema],
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    total: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        enum: ['pending', 'accepted', 'delivered'],
        default: 'pending'
    },
    email: {
        type: String,
        required: true
    }
})


var OrderSchema = module.exports = mongoose.model('Order', OrderSchema);