const expect = require("chai").expect;
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");

chai.use(chaiHttp);
let token
before(() => {
    it('Register User ', async () => {
        let res = await addUser({
            "email": "newUser@gmail.com",
            "password": "newUser"
        })
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
        token = await loginUser({ email: "newUser@gmail.com" })
    })
})

const loginUser = async (user) => {
    return await chai.request(server)
        .post(`/api/login`)
        .send(user)
}

const listUsers = async (token) => {
    return await chai.request(server)
        .get(`/api/users`)
        .set({ 'access_token': token })
        .send({})
}

const getUser = async (user, token) => {
    return await chai.request(server)
        .get(`/api/user/${user}`)
        .set({ 'access_token': token })
        .send({})
}
const addUser = async (user) => {
    return await chai.request(server)
        .post(`/api/user`)
        .send(user)
}

const updateUser = async (user, data, token) => {
    return await chai.request(server)
        .put(`/api/user/${user}`)
        .set({ 'access_token': token })
        .send(data)
}
const deleteUser = async (user, token) => {
    return await chai.request(server)
        .delete(`/api/user/${user}`)
        .set({ 'access_token': token })
        .send({})
}

const listFood = async (token) => {
    return await chai.request(server)
        .get(`/api/allFood`)
        .set({ 'access_token': token })
        .send({})
}

const getFood = async (name, token) => {
    return await chai.request(server)
        .get(`/api/food/${name}`)
        .set({ 'access_token': token })
        .send({})
}
const addFood = async (food, token) => {
    return await chai.request(server)
        .post(`/api/food`)
        .set({ 'access_token': token })
        .send(food)
}

const updateFood = async (name, data, token) => {
    return await chai.request(server)
        .put(`/api/food/${name}`)
        .set({ 'access_token': token })
        .send(data)
}
const deleteFood = async (name, token) => {
    return await chai.request(server)
        .delete(`/api/food/${name}`)
        .set({ 'access_token': token })
        .send({})
}

const saveOrder = async (order, token) => {
    return await chai.request(server)
        .post(`/api/order`)
        .set({ 'access_token': token })
        .send(order)
}

const getOrderById = async (id, token) => {
    return await chai.request(server)
        .get(`/api/order/${id}`)
        .set({ 'access_token': token })
        .send({})
}
const getAllOrders = async ( token) => {
    return await chai.request(server)
        .get(`/api/orders`)
        .set({ 'access_token': token })
        .send({})
}
const deleteOrder = async (id, token) => {
    return await chai.request(server)
        .delete(`/api/order/${id}`)
        .set({ 'access_token': token })
        .send({})
}
describe('Success Scenarios', () => {
    it('List all users ', async () => {
        let res = await listUsers(token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object')
    })
    it('Get an User ', async () => {
        let res = await getUser(`newUser@gmail.com`, token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object')
    })
    it('Update an User ', async () => {
        let res = await updateUser(`newUser@gmail.com`, { passowrd: "passChnaged" }, token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object')
    })
    it('Delete an User ', async () => {
        let res = await deleteUser(`newUser@gmail.com`, token.body.token)
        expect(res.status).equal(204);
    })
    it('Add Food ', async () => {
        let res = await addFood({
            "name": "DummyFoodName",
            "style": "Chinese",
            "spiceLevel": "high",
            "cost": 250,
            "isVegan":false
        },token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object');
    })
    it('List all food ', async () => {
        let res = await listFood(token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object')
    })
    it('Get Food ', async () => {
        let res = await getFood(`DummyFoodName`, token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object')
    })
    it('Update Food ', async () => {
        let res = await updateFood(`DummyFoodName`, { cost: 300 }, token.body.token)
        expect(res.status).equal(200);
        expect(res.body).to.be.an('object')
    })

    it('Order ', async () => {
        let resp = await saveOrder([
            {
                "name": "DummyFoodName",
                "quantity": 3
            }
        ],token.body.token)
        let orderId = resp.body.Orders._id
        await getAllOrders(token.body.token)
        await getOrderById(orderId, token.body.token)
        let res = await deleteOrder(orderId, token.body.token)
        expect(res.status).equal(204);
    })
    it('Delete Food ', async () => {
        let res = await deleteFood(`DummyFoodName`, token.body.token)
        expect(res.status).equal(204);
    })
})


describe('Failure Scenarios', () => {
    it('Get an User ', async () => {
        let res = await getUser(`dummy@gmail.com`, token.body.token)
        expect(res.status).equal(404);
    })
    it('Update an User ', async () => {
        let res = await updateUser(`dummy@gmail.com`, { passowrd: "passChnaged" }, token.body.token)
        expect(res.status).equal(404);
    })
    it('Delete an User ', async () => {
        let res = await deleteUser(`dummy@gmail.com`, token.body.token)
        expect(res.status).equal(404);
    })

    it('Invalid token ', async () => {
        let res = await deleteUser(`dummy@gmail.com`, null)
        expect(res.status).equal(500);
    })

    it('Add user without proper details ', async () => {
        let res = await addUser({
            "email": "newUser@gmail.com"
        })
        expect(res.status).equal(400);
    })
    it('Invalid user login', async () => {
        let res = await loginUser({ email: "newUser@gmail.com" })
        expect(res.status).equal(404);
    })

})